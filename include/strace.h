/*
** strace.h for include in /home/chapea_o/travail/tek2/unixav/strace/strace/include
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Wed Apr 24 20:34:42 2013 olivier chapeau
** Last update Fri May 03 12:12:41 2013 olivier chapeau
*/

#ifndef STRACE_H_
# define STRACE_H_

# define N_SYSCALLS	(512 + 1)

typedef enum	e_bool
{
  FALSE,
  TRUE
}		t_bool;

typedef enum	e_types
{
  SHORT,
  INT,
  LONG,
  DOUBLE,

}		t_types;

typedef struct	s_syscall
{
  int		num;
  char		*name;
  int		nb_args;
}		t_syscall;

#endif /* !STRACE_H_ */

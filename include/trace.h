/*
** trace.h for include in /home/chapea_o/travail/tek2/unixav/strace/strace/include
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Wed May 01 15:53:52 2013 olivier chapeau
** Last update Thu May 02 17:44:10 2013 olivier chapeau
*/

#ifndef TRACE_H_
# define TRACE_H_

# include <sys/user.h>
# include "options.h"
# include "strace.h"

typedef	struct			s_trace
{
  long				enter;
  long				opcode;
  unsigned long			prev_rip;
  struct user_regs_struct	regs;
}				t_trace;

void	init_trace(t_trace *trace);
int	create_pid(t_options *options);
int	attach_pid(t_options *options);
int	loop_trace(pid_t pid, t_syscall *syscalls);

#endif /* !TRACE_H_ */

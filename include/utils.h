/*
** utils.h for include in /home/chapea_o/travail/tek2/unixav/strace/strace/include
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Wed Apr 24 21:42:35 2013 olivier chapeau
** Last update Thu May 02 16:43:58 2013 olivier chapeau
*/

#ifndef UTILS_H_
# define UTILS_H_

typedef enum	e_usage
{
  FAILURE,
  HELP
}		t_usage;

int	usage(char **argv, t_usage flag);
int	quit(const char *msg);
int	error(const char *msg);
char	*epurstr(const char *src, char *dest);

#endif /* !UTILS_H_ */

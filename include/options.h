/*
** options.h for include in /home/chapea_o/travail/tek2/unixav/strace/strace/include
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Wed Apr 24 20:34:26 2013 olivier chapeau
** Last update Fri May 03 19:38:55 2013 olivier chapeau
*/

#ifndef OPTIONS_H_
# define OPTIONS_H_

# include <unistd.h>
# include "strace.h"

typedef struct	s_options
{
  t_bool	attach;
  pid_t		pid;
  char		**argv;
}		t_options;

int	initopt(int argc, char **argv, t_options *options);
void	freeopt(t_options *options);

#endif /* !OPTIONS_H_ */

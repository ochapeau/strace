/*
** parser.h for include in /home/chapea_o/travail/tek2/unixav/strace/strace/include
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Thu May 02 14:02:55 2013 olivier chapeau
** Last update Thu May 02 18:08:50 2013 olivier chapeau
*/

#ifndef PARSER_H_
# define PARSER_H_

# include "strace.h"

# define BEGIN 		"#define __NR_"
# define BEGIN_LEN   	(13)

void	init_syscalls(t_syscall *syscalls);
int	parse_file(const char *filename, t_syscall *syscalls);
void	free_syscalls(t_syscall *syscalls);

#endif /* !PARSER_H_ */

/*
** main.c for src in /home/chapea_o/travail/tek2/unixav/strace/strace/src
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Tue Apr 23 21:08:01 2013 olivier chapeau
** Last update Thu May 09 16:43:33 2013 olivier chapeau
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ptrace.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include "strace.h"
#include "options.h"
#include "trace.h"
#include "parser.h"

static t_options	g_options;

static void		sighandler(int signum)
{
  (void) signum;
  printf("Process killed by Ctrl-C.\n");
  if (g_options.attach == TRUE)
  {
    if (ptrace(PTRACE_DETACH, g_options.pid, NULL, NULL) == -1)
    {
      fprintf(stderr, "ptrace(PTRACE_DETACH): %s.\n", strerror(errno));
      exit(EXIT_FAILURE);
    }
    printf("Process %d detached.\n", g_options.pid);
  }
  exit(EXIT_SUCCESS);
}

int		main(int argc, char **argv)
{
  t_syscall	syscalls[N_SYSCALLS];

  if (signal(SIGINT, &sighandler) == SIG_ERR)
  {
    fprintf(stderr, "signal: %s.\n", strerror(errno));
    return (EXIT_FAILURE);
  }
  init_syscalls(syscalls);
  if (parse_file("/usr/include/asm-x86/unistd_64.h", syscalls) != 0)
    return (EXIT_FAILURE);
  if (initopt(argc, argv, &g_options) != 0)
    return (EXIT_FAILURE);
  if (g_options.attach == TRUE)
  {
    if (attach_pid(&g_options) != 0)
      return (EXIT_FAILURE);
  }
  else
  {
    if (create_pid(&g_options) != 0)
      return (EXIT_FAILURE);
  }
  if (loop_trace(g_options.pid, syscalls) != 0)
    return (EXIT_FAILURE);
  freeopt(&g_options);
  free_syscalls(syscalls);
  return (EXIT_SUCCESS);
}

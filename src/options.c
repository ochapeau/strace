/*
** options.c for src in /home/chapea_o/travail/tek2/unixav/strace/strace/src
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Wed Apr 24 21:35:09 2013 olivier chapeau
** Last update Wed May 01 18:27:07 2013 olivier chapeau
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "options.h"
#include "utils.h"

static int	initargv(int argc, char **argv, t_options *options, int optind)
{
  int		i;

  i = 0;
  if ((options->argv = malloc((argc - optind + 1) * sizeof(char *))) == NULL)
    return (error("malloc"));
  if (optind < argc)
  {
    while (optind < argc)
      options->argv[i++] = argv[optind++];
    options->argv[i] = NULL;
  }
  return (0);
}

/* L'utilisation de getopt empeche l'utilisation des options sur les params */
/* par exemple ./strace ls -l */
/* Fonction eventuellement a revoir */
int	initopt(int argc, char **argv, t_options *options)
{
  int	opt;

  options->attach = FALSE;
  options->pid = -1;
  options->argv = NULL;
  if (argc == 1)
      return (usage(argv, FAILURE));
  while ((opt = getopt(argc, argv, "hp:")) != -1)
  {
    if (opt == 'p')
    {
      options->attach = TRUE;
      options->pid = (pid_t)atoi(optarg);
    }
    if (opt == 'h')
      return (usage(argv, HELP));
    if (opt == '?')
      return (usage(argv, FAILURE));
  }
  if (optind != argc)
    initargv(argc, argv, options, optind);
  return (0);
}

void	freeopt(t_options *options)
{
  if (options != NULL)
    free(options->argv);
}

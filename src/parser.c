/*
** parser.c for src in /home/chapea_o/travail/tek2/unixav/strace/strace/src
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Thu May 02 11:18:26 2013 olivier chapeau
** Last update Thu May 09 11:27:10 2013 olivier chapeau
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "parser.h"
#include "utils.h"

void	init_syscalls(t_syscall *syscalls)
{
  int		i;

  i = 0;
  while (i < N_SYSCALLS)
  {
    syscalls[i].num = -1;
    syscalls[i].name = NULL;
    syscalls[i].nb_args = -1;
    i++;
  }
}

void	free_syscalls(t_syscall *syscalls)
{
  int	i;

  i = 0;
  while (i < N_SYSCALLS)
  {
    free(syscalls[i].name);
    i++;
  }
}

static int	parse_line(char *line, int *max_num, t_syscall *syscalls)
{
  char		*tmp;
  char		*name;
  char		*snum;
  int		num;

  line[strlen(line + BEGIN_LEN) + BEGIN_LEN - 1] = '\0';
  if ((tmp = malloc(strlen(line + BEGIN_LEN))) == NULL)
    return (error("malloc"));
  bzero(tmp, strlen(line + BEGIN_LEN));
  (void) epurstr(line + BEGIN_LEN, tmp);
  if ((name = strtok(tmp, " ")) == NULL)
    return (error("strtok"));
  if ((snum = strtok(NULL, " ")) == NULL)
    return (error("strtok"));
  num = atoi(snum);
  if (num > *max_num + 1 || num >= N_SYSCALLS - 1)
    return (error("numero de syscall incorrect."));
  *max_num = num > *max_num ? num : *max_num;
  syscalls[num].num = num;
  if ((syscalls[num].name = strdup(name)) == NULL)
    return (error("strdup"));
  free(tmp);
  return (0);
}

int		parse_file(const char *filename, t_syscall *syscalls)
{
  FILE		*fp;
  char		*line;
  int		max_num;
  size_t	len;

  line = NULL;
  len = 0;
  max_num = 0;
  if ((fp = fopen(filename, "r")) == NULL)
    return (error("fopen"));
  while (getline(&line, &len, fp) != -1)
  {
    if (!strncmp(BEGIN, line, BEGIN_LEN))
    {
      if (parse_line(line, &max_num, syscalls) != 0)
	return (-1);
    }
  }
  free(line);
  if (fclose(fp) == EOF)
    return (error("fclose"));
  if (syscalls[0].name == NULL)
    return (error("fichier malforme.\n"));
  return (0);
}

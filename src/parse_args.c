/*
** parse_args.c for src in /home/chapea_o/travail/tek2/unixav/strace/strace/src
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Thu May 09 11:28:36 2013 olivier chapeau
** Last update Thu May 09 11:28:36 2013 olivier chapeau
*/

#include "parse_args.h"

int		parse_file(const char *filename, t_syscall *syscalls)
{
  FILE		*fp;
  char		*line;
  int		max_num;
  size_t	len;

  line = NULL;
  len = 0;
  max_num = 0;
  if ((fp = fopen(filename, "r")) == NULL)
    return (error("fopen"));
  while (getline(&line, &len, fp) != -1)
  {
    if (!strncmp(BEGIN, line, BEGIN_LEN))
    {
      if (parse_line(line, &max_num, syscalls) != 0)
	return (-1);
    }
  }
  free(line);
  if (fclose(fp) == EOF)
    return (error("fclose"));
  if (syscalls[0].name == NULL)
    return (error("fichier malforme.\n"));
  return (0);
}

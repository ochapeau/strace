/*
** utils.c for src in /home/chapea_o/travail/tek2/unixav/strace/strace/src
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Wed Apr 24 21:39:23 2013 olivier chapeau
** Last update Thu May 02 17:10:47 2013 olivier chapeau
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include "utils.h"

int	usage(char **argv, t_usage flag)
{
  if (flag == HELP)
  {
      printf("strace - a syscall tracer\n");
      printf("Usage: %s [-p pid] or [cmd [opts...]].\n", argv[0]);
  }
  else
  {
      fprintf(stderr, "Usage: %s [-p pid] or [cmd [opts...]].\n", argv[0]);
  }
  return (1);
}

int	quit(const char *msg)
{
  printf("%s.\n", msg);
  return (EXIT_SUCCESS);
}

int	error(const char *msg)
{
  fprintf(stderr, "%s: %s.\n", msg, strerror(errno));
  return (-1);
}

char	*epurstr(const char *src, char *dest)
{
  int	i;
  int	j;

  i = 0;
  j = 0;
  while (src && src[i] != '\0')
  {
    while (src[i] == ' ' || src[i] == '\t')
      i++;
    while (src[i] >= '!' && src[i] <= '~')
    {
      dest[j++] = src[i];
      i++;
    }
    while (src[i] == ' ' || src[i] == '\t')
      i++;
    if (src[i] != '\0')
      dest[j++] = ' ';
  }
  dest[j] = '\0';
  return (dest);
}

/*
** trace.c for src in /home/chapea_o/travail/tek2/unixav/strace/strace/src
** 
** Made by olivier chapeau
** Login   <chapea_o@epitech.net>
** 
** Started on  Wed May 01 15:42:17 2013 olivier chapeau
** Last update Thu May 09 16:59:26 2013 olivier chapeau
*/

#include <unistd.h>
#include <sys/ptrace.h>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <sys/wait.h>
#include <string.h>
#include <stdio.h>
#include "utils.h"
#include "trace.h"

int	create_pid(t_options *options)
{
  if ((options->pid = fork()) == -1)
    return (error("fork"));
  else if (options->pid == 0)
  {
    if (ptrace(PTRACE_TRACEME, 0, NULL, NULL) == -1)
      return (error("ptrace(PTRACE_TRACEME)"));
    if (execvp(options->argv[0], &options->argv[0]) == -1)
      return (error("execvp"));
  }
  return (0);
}

int	attach_pid(t_options *options)
{
    if (ptrace(PTRACE_ATTACH, options->pid, NULL, NULL) == -1)
      return (error("ptrace(PTRACE_ATTACH)"));
    printf("Process %d attached.\n", options->pid);
    if (ptrace(PTRACE_SINGLESTEP, options->pid, NULL, NULL) == -1)
      return (error("ptrace(PTRACE_SINGLESTEP)"));
    return (0);
}

static int	trace_pid(pid_t pid, t_syscall *syscalls, t_trace *infos)
{
  if (ptrace(PTRACE_GETREGS, pid, NULL, &infos->regs) == -1)
    return (error("ptrace(PTRACE_GETREGS)"));
  if ((infos->opcode =
	ptrace(PTRACE_PEEKTEXT, pid, infos->regs.rip, NULL)) == -1)
    return (error("ptrace(PTRACE_PEEKTEXT)"));
  infos->opcode &= 0xFFFF;
  if (infos->enter == 0
      && (infos->opcode == 0x050F	//SYSCALL
	|| infos->opcode == 0x80CD	//INT 0x80
	|| infos->opcode == 0x340F))	//SYSENTER
  {
    infos->enter = infos->regs.rax;
    infos->prev_rip = infos->regs.rip;
  }
  if (infos->enter != 0 && infos->prev_rip != infos->regs.rip)
  {
    if (syscalls && syscalls[infos->enter].name != NULL)
    {
      // Function display
      printf("%s() = ", syscalls[infos->enter].name);
      if (infos->regs.rax > 2147647483)
	printf("0x%lx\n", infos->regs.rax);
      else
	printf("%d\n", (int)infos->regs.rax);
    }
    infos->enter = 0;
  }
  if (ptrace(PTRACE_SINGLESTEP, pid, NULL, NULL) == -1)
    return (error("ptrace(PTRACE_SINGLESTEP)"));
  return (0);
}

int		loop_trace(pid_t pid, t_syscall *syscalls)
{
  int		w;
  int		status;
  t_trace	infos;

  status = 0;
  infos.enter = 0;
  infos.prev_rip = 0;
  while ((w = wait4(pid, &status, 0, NULL)) != -1)
  {
    if (WIFEXITED(status))
      return (quit("Exited normally"));
    if (WIFSIGNALED(status))
      return (quit("Exited by a signal"));
    if (WIFSTOPPED(status))
    {
      if (WSTOPSIG(status) == SIGTRAP)
	if (trace_pid(pid, syscalls, &infos) != 0)
	  return (-1);
    }
  }
  if (w == -1)
    return (error("wait4"));
  return (0);
}


##
## Makefile for strace in /home/chapea_o/travail/tek2/unixav/strace/strace
##
## Made by olivier chapeau
## Login   <chapea_o@epitech.net>
##
## Started on  Tue Apr 23 21:00:59 2013 olivier chapeau
## Last update Thu May 02 14:00:39 2013 olivier chapeau
##

SOURCES	= src/

INCLUDE	= include/

CC		= gcc

CFLAGS	= -Wall -Wextra -Werror -I$(INCLUDE) -g

NAME	= strace

SRCS	= \
		  $(SOURCES)main.c\
		  $(SOURCES)options.c\
		  $(SOURCES)trace.c\
		  $(SOURCES)parser.c\
		  $(SOURCES)utils.c

OBJS	= $(SRCS:.c=.o)

RM		= rm -rf

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(NAME)

clean:
	$(RM) $(OBJS)

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re
